Tasks = new Mongo.Collection("tasks");

if (Meteor.isServer) {
    // Only publish tasks that are public or belong to the current user
    Meteor.publish("tasks", function () {
        return Tasks.find({
            $or: [
                { private: {$ne: true} }
            ]
        }, {sort: {date_created: -1}});
    });
}

if (Meteor.isClient) {
    // This code only runs on the client
    Meteor.subscribe("tasks");

    Template.header.rendered = function() {
        var hellos = ["Salut", "Hallo", "Ahoj", "Hej", "Tere", "Hey", "Kon'nichiwa", "Shalom", "Olá", "Privet"];
        $('#hello').text(hellos[Math.floor(Math.random()*hellos.length)]);
    }

    Template.body.helpers({
        tasks: function () {
            if (Session.get("hideCompleted")) {
                // If hide completed is checked, filter tasks
                return Tasks.find({checked: {$ne: true}}, {sort: {createdAt: -1}});
            } else {
                // Otherwise, return all of the tasks
                return Tasks.find({}, {sort: {createdAt: -1}});
            }
        },
        hideCompleted: function () {
            return Session.get("hideCompleted");
        },
        incompleteCount: function () {
            return Tasks.find({checked: {$ne: true}}).count();
        },
        completeCount: function () {
            return Tasks.find({checked: true}).count();
        }
    });

    Template.body.events({
        "submit .new-task": function (event) {
            // Prevent default browser form submit
            event.preventDefault();

            // Get value from form element
            var text = event.target.text.value;

            // Go and add a task named 'gorgoroth'
            if (text == "gorgoroth") {
              var audio = new Audio('sign.mp3');
              audio.play();
            }

            // Insert a task into the collection
            Meteor.call("addTask", text);

            // Clear form
            event.target.text.value = "";
        },
        "change .hide-completed input": function (event) {
            Session.set("hideCompleted", event.target.checked);
        }
    });

    Template.task.events({
        "click .toggle-checked": function () {
            // Set the checked property to the opposite of its current value
            Meteor.call("setChecked", this._id, ! this.checked);
        },
        "click .delete": function () {
            Meteor.call("deleteTask", this._id);
        }
    });

}

Meteor.methods({
    addTask: function (text) {
        Tasks.insert({
            text: text,
            createdAt: new Date()
        });
    },
    deleteTask: function (taskId) {
        var task = Tasks.findOne(taskId);
        Tasks.remove(taskId);
    },
    setChecked: function (taskId, setChecked) {
        var task = Tasks.findOne(taskId);
        Tasks.update(taskId, { $set: { checked: setChecked} });
    }
});
